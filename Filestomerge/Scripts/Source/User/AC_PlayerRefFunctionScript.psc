Scriptname AC_PlayerRefFunctionScript extends ReferenceAlias

;************************Properties**********************

SoundCategory Property audioCategoryVoc Auto Mandatory

Potion Property Psycho Auto Mandatory
Potion Property RadAway Auto Mandatory 
Potion Property MedX Auto Mandatory
Potion Property Jet Auto Mandatory
Potion Property UltraJet Auto Mandatory
Potion Property BuffJet Auto Mandatory
Potion Property JetFuel Auto Mandatory
Potion Property PsychoJet Auto Mandatory 
Potion Property DLC04SmoothOperator Auto Mandatory  

Potion Property AC_Jet1 Auto Mandatory
Potion Property AC_JetChemEffect Auto Mandatory
Potion Property AC_UltraJet Auto Mandatory
Potion Property AC_BuffJet Auto Mandatory
Potion Property AC_JetFuel Auto Mandatory
Potion Property AC_PsychoJet Auto Mandatory 

Idle Property AC_Psycho1stP Auto Mandatory
Idle Property AC_MedX1stP Auto Mandatory
Idle Property AC_Jet1stP Auto Mandatory
Idle Property AC_Pill1stP Auto Mandatory
Idle Property AC_RADX1stP Auto Mandatory

Idle Property AC_Psycho3rdP Auto Mandatory
Idle Property AC_MedX3rdP Auto Mandatory
Idle Property AC_Jet3rdP Auto Mandatory
Idle Property AC_Pill3rdP Auto Mandatory
Idle Property AC_RADX3rdP Auto Mandatory
Idle Property AC_Psycho2_3rdP Auto Mandatory 
Idle Property AC_Radaway1stP Auto Mandatory
Idle Property AC_Radaway3rdP Auto Mandatory

Keyword Property PlayerConsumePsycho Auto Mandatory

FormList Property AC_PillList Auto Mandatory
FormList Property AC_MedXList Auto Mandatory
FormList Property AC_MentatsList Auto Mandatory
FormList Property AC_PsychoList Auto Mandatory

;***********************Variables************************

Actor PlayerREF
Bool Busy = False
Bool WeaponWasOut

;***********************Events***************************

Event OnInit()
	PlayerREF = Game.getPlayer()
    ;AddInventoryEventFilter(Jet)
EndEvent

Event OnItemEquipped(Form akBaseObject, ObjectReference akReference)
			
		;;Psycho==================================================================================================
		if AC_PsychoList.HasForm(akBaseObject) && Busy == False
			audioCategoryVoc.Mute()
			Debug.Trace("Muting Player audio")
			Busy = True
			Utility.Wait(0.5)	
				If PlayerRef.GetAnimationVariableBool("IsFirstPerson") == True
					PlayerREf.PlayIdle(AC_Psycho1stP)
					Utility.Wait(1.9)
					Debug.Trace("UnMuting Player audio")
					audioCategoryVoc.UnMute() 
					PlayerREF.SayCustom(PlayerConsumePsycho)
					Busy = False
				Elseif PlayerRef.GetAnimationVariableBool("IsFirstPerson") == False
					Debug.Notification("Psycho 3rdP")
					PlayerREf.PlayIdle(AC_Psycho3rdP)
					Utility.Wait(1.9)
					Debug.Trace("UnMuting Player audio")
					audioCategoryVoc.UnMute() 
					PlayerREF.SayCustom(PlayerConsumePsycho)
					Busy = False
				EndIf
		;;Psycho Jet==================================================================================================
		ElseIf akBaseObject== PsychoJet && Busy == False
			audioCategoryVoc.Mute()
			Debug.Trace("Muting Player audio")
			Busy = True
			Utility.Wait(0.5)	
				If PlayerRef.GetAnimationVariableBool("IsFirstPerson") == True
					PlayerREf.PlayIdle(AC_Psycho1stP)
					Utility.Wait(2.0)
					Debug.Trace("UnMuting Player audio")
					audioCategoryVoc.UnMute() 
					;PlayerREF.SayCustom(PlayerConsumePsycho)
					PlayerREF.EquipItem(AC_PsychoJet, false, True)
					Busy = False
				Elseif PlayerRef.GetAnimationVariableBool("IsFirstPerson") == False
					Debug.Notification("Psycho 3rdP")
					PlayerREf.PlayIdle(AC_Psycho2_3rdP)
					Utility.Wait(1.2)
					Debug.Trace("UnMuting Player audio")
					audioCategoryVoc.UnMute() 
					;PlayerREF.SayCustom(PlayerConsumePsycho)
					PlayerREF.EquipItem(AC_PsychoJet, false, True)
					Busy = False
				EndIf		
		;;MedX=====================================================================================================
		ElseIf AC_MedXList.HasForm(akBaseObject) && Busy == False
			Busy = True
			Utility.Wait(0.5)
				If PlayerRef.GetAnimationVariableBool("IsFirstPerson") == True
					PlayerREf.PlayIdle(AC_MedX1stP)
					Utility.Wait(0.5)
					Busy = False
				ElseIf PlayerRef.GetAnimationVariableBool("IsFirstPerson") == False
					PlayerREf.PlayIdle(AC_MedX3rdP)
					Utility.Wait(0.5)
					Busy = False
				EndIf
		;;RadAway=====================================================================================================
		ElseIf akBaseObject == RadAway && Busy == False 
			Busy = True
			Utility.Wait(0.5)				
				If PlayerRef.GetAnimationVariableBool("IsFirstPerson") == True
					PlayerREf.PlayIdle(AC_Radaway1stP)
					Utility.Wait(2.0)
					Busy = False
				ElseIf PlayerRef.GetAnimationVariableBool("IsFirstPerson") == False
					PlayerREf.PlayIdle(AC_Radaway3rdP)
					Utility.Wait(2.0)
					Busy = False
				EndIf	
		;;Pills====================================================================================================
		ElseIf AC_PillList.HasForm(akBaseObject) && Busy == False
			Busy = True
			Utility.Wait(0.5)
				If PlayerRef.GetAnimationVariableBool("IsFirstPerson") == True
					PlayerREf.PlayIdle(AC_Pill1stP)
					Utility.Wait(0.5)
					Busy = False
				ElseIf PlayerRef.GetAnimationVariableBool("IsFirstPerson") == False
					PlayerREf.PlayIdle(AC_Pill3rdP)
					Utility.Wait(0.5)
					Busy = False
				EndIf
		;;Mentats====================================================================================================
		ElseIf AC_MentatsList.HasForm(akBaseObject) && Busy == False
			Busy = True
			Utility.Wait(0.5)
				If PlayerRef.GetAnimationVariableBool("IsFirstPerson") == True
					PlayerREf.PlayIdle(AC_RADX1stP)
					Utility.Wait(0.5)
					Busy = False
				ElseIf PlayerRef.GetAnimationVariableBool("IsFirstPerson") == False
					PlayerREf.PlayIdle(AC_RADX3rdP)
					Utility.Wait(0.5)
					Busy = False
				EndIf
		;;Jet====================================================================================================
		ElseIf akBaseObject == Jet && Busy == False 
			Busy = True
			Utility.Wait(0.5)				
				If PlayerRef.GetAnimationVariableBool("IsFirstPerson") == True
					PlayerREf.PlayIdle(AC_Jet1stP)
					Utility.Wait(2.0)
					PlayerREF.EquipItem(AC_JetChemEffect, false, True)
					Busy = False
				ElseIf PlayerRef.GetAnimationVariableBool("IsFirstPerson") == False
					PlayerREf.PlayIdle(AC_Jet3rdP)
					Utility.Wait(2.0)
					PlayerREF.EquipItem(AC_JetChemEffect, false, True)
					Busy = False
				EndIf
		;;Ultra Jet====================================================================================================
		ElseIf akBaseObject == UltraJet && Busy == False 
			Busy = True
			Utility.Wait(0.5)				
				If PlayerRef.GetAnimationVariableBool("IsFirstPerson") == True
					PlayerREf.PlayIdle(AC_Jet1stP)
					Utility.Wait(2.0)
					PlayerREF.EquipItem(AC_UltraJet, false, True)
					Busy = False
				ElseIf PlayerRef.GetAnimationVariableBool("IsFirstPerson") == False
					PlayerREf.PlayIdle(AC_Jet3rdP)
					Utility.Wait(2.0)
					PlayerREF.EquipItem(AC_UltraJet, false, True)
					Busy = False
				EndIf
		;;buffJet====================================================================================================
		ElseIf akBaseObject == BuffJet && Busy == False 
			Busy = True
			Utility.Wait(0.5)
				If PlayerRef.GetAnimationVariableBool("IsFirstPerson") == True	
					PlayerREf.PlayIdle(AC_Pill1stP)
					Utility.Wait(1.8)
					PlayerREF.EquipItem(AC_buffJet, false, True)
					Busy = False
				ElseIf PlayerRef.GetAnimationVariableBool("IsFirstPerson") == True
					PlayerREf.PlayIdle(AC_Pill3rdP)
					Utility.Wait(1.8)
					PlayerREF.EquipItem(AC_buffJet, false, True)
					Busy = False
				EndIf
		;;Jet Fuel====================================================================================================
		ElseIf akBaseObject == JetFuel || akBaseObject == DLC04SmoothOperator && Busy == False 
			Busy = True
			Utility.Wait(0.5)				
				If PlayerRef.GetAnimationVariableBool("IsFirstPerson") == True
					PlayerREf.PlayIdle(AC_Jet1stP)
					Utility.Wait(0.5)
					Busy = False
				ElseIf PlayerRef.GetAnimationVariableBool("IsFirstPerson") == False
					PlayerREf.PlayIdle(AC_Jet3rdP)
					Utility.Wait(0.5)
					Busy = False
				EndIf
		
		EndIf
EndEvent


;too many Compatability issues for now
; Event OnItemAdded(Form akBaseItem, int aiItemCount, ObjectReference akItemReference, ObjectReference akSourceContainer)
;     if akBaseItem == Jet
;     	PlayerREF.RemoveItem(Jet, aiItemCount, True, None)
;     	PlayerREF.AddItem(AC_Jet1, aiItemCount, True)
;     EndIf
; EndEvent

;*********************Functions*************************

