;NotSure700

Scriptname TheFridgeScript extends ObjectReference

CoolingRecipe[] Property CoolingRecipes  mandatory auto const

float cooldown = 1.0

Event OnClose(ObjectReference akActionRef)
	CancelTimerGameTime()
	if isPowered()
		StartTimerGameTime(cooldown)	
	endif
EndEvent

Event OnPowerOn(ObjectReference akPowerGenerator)
	CancelTimerGameTime()
	if isPowered()
		StartTimerGameTime(cooldown)	
	endif
EndEvent

Event OnPowerOff()
	CancelTimerGameTime()
EndEvent

Event OnTimerGameTime(int aiTimerID)		
	if isPowered()
		CheckForCooling()
	endif
EndEvent

Function CheckForCooling()
	int i = 0
	int count = CoolingRecipes.Length
	int itemCount
	while i < count
		itemCount = GetItemCount(CoolingRecipes[i].WarmDrinkVariant)
		if itemCount > 0 && CoolingRecipes[i].ColdDrinkVariant != NONE
			RemoveItem(CoolingRecipes[i].WarmDrinkVariant, itemCount, abSilent = True)
			AddItem(CoolingRecipes[i].ColdDrinkVariant, itemCount, abSilent = True)
		endif
		i += 1
	endWhile
EndFunction

Struct CoolingRecipe
	potion WarmDrinkVariant
	potion ColdDrinkVariant
EndStruct