Scriptname AC_JetDelayScript extends activemagiceffect


;Idle Property AA_IdleUseJet Auto
Spell Property AC_JetSlowTimeSpell Auto

Event OnEffectStart(actor akTarget, actor akCaster)
	utility.wait(0.4)
	;akTarget.PlayIdle(AA_IdleUseJet)
	utility.wait(3)
	AC_JetSlowTimeSpell.Cast(akTarget)
EndEvent